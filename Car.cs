using System;
using System.Collections.Generic;

public class Car<I1, I2> : List<Tuple<I1, I2>>
{
    public void addCar(I1 _I1, I2 _I2 )
    {
        Add(new Tuple<I1, I2>(_I1, _I2));
    }
}
