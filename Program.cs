﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

namespace _5002_Assn3_GR2_Team6
{
    class Program
    {
        static void Main(string[] args)
        {
            Start:
            
            //Start the program with Clear();
            Console.Clear();
            var selection = 0;

            do {
                Console.Clear();
                Console.WriteLine("_____________________________________________________");
                Console.WriteLine("|                        Menu                        |");
                Console.WriteLine("|____________________________________________________|");
                Console.WriteLine("|                                                    |");
                Console.WriteLine("|      1) Display A Countdown from 99 to 0           |");
                Console.WriteLine("|      2) Count the letters in a string              |");
                Console.WriteLine("|      3) A dictionary with fruits & vegetables      |");
                Console.WriteLine("|      4) A List that holds car makes & their models |");
                Console.WriteLine("|      5) You Can Stop Program                       |");
                Console.WriteLine("|____________________________________________________|");
                Console.WriteLine();
                Console.WriteLine("Choose a number:");
                var t = Console.ReadLine();
                bool checkInput = int.TryParse(t, out selection);
                
                if (checkInput)
                {
                    switch (selection)
                    {
                        case 1:
                            Console.WriteLine($"You selected option {selection}");

                            var counter = 0; 
                            for (var i = 100; i > counter; i--)
                            {
                                var a = i - 1; 
                                Console.WriteLine(a);
                            }

                            Console.WriteLine("Press <Enter> to return to menu"); 
                            Console.ReadKey();
                        break;

                        case 2:
                            Console.WriteLine($"You selected option {selection}");

                            //Declare variables
                            int delay = 1000;
                            var qString = "";
                            var stringCount = 0;
                            var loopCount = 0;
                            var stringList = new List<string>{};

                            Console.Clear();
                            Console.WriteLine($"You selected option {selection}\n");
                            
                            //Request user input
                            Console.WriteLine("Please enter a string");
                            qString = Console.ReadLine();
                            stringCount = qString.Length;
                            loopCount = CountWords($"{qString}");


                            //Displays number of characters in "Gorillaz" (8)
                            Console.WriteLine($"{qString} has {stringCount} characters\n");
                            Thread.Sleep(delay);//Creates a delay

                            Console.WriteLine("Creating a list with each word of the string...\n");
                            Thread.Sleep(delay);

                            //Adds items to list (stringList)
                            var output = qString.Split(' ');
                            for (var i = 0; i < loopCount; i++)
                            {
                                stringList.Add(output[i]);
                            }

                            Console.WriteLine($"Printing list...\n");
                            Thread.Sleep(delay);

                            //Display stringList
                            stringList.ForEach(Console.WriteLine);

                            //Method to split string inorder to obtain the 'loopCount' variable
                            //enabling me to count how many words are in the string 'qString'
                            int CountWords(string test)
                            {
                                int count = 0;
                                bool wasInWord = false;
                                bool inWord = false;

                                    for (int i = 0; i < test.Length; i++)
                                    {
                                        if (inWord)
                                        {
                                            wasInWord = true;
                                        }

                                        //if char has a space add 1 to 'count'
                                        if (Char.IsWhiteSpace(test[i]))
                                        {
                                            if (wasInWord)
                                            {
                                                count++;
                                                wasInWord = false;
                                            }
                                            inWord = false;
                                        }
                                        else
                                        {
                                            inWord = true;
                                        }
                                    }
                                // Check to see if we got out with seeing a word
                                if (wasInWord)
                                {
                                        count++;
                                }
                                return count;
                            }
                            Thread.Sleep(delay);

                            Console.WriteLine("\nPress <Enter> to return to menu"); 
                            Console.ReadKey();
                        break;

                        case 3:// This is Question 3, each case number should reflect the question
                            Console.WriteLine($"You selected option {selection}");
                            
                            Dictionary<string, string> fruitAndVegetables = new Dictionary<string, string>()
                            {
                                {"Apple", "Fruit"},
                                {"Banana", "Fruit"},
                                {"Mango", "Fruit"},
                                {"Onion", "Vegetable"},
                                {"Pumpkin", "Vegetable"},
                                {"Carrot", "Vegetable"},
                                {"Peach", "Fruit"},
                                {"Apricot", "Fruit"},
                                {"Plum", "Fruit"},
                                {"Pear", "Fruit"},
                                {"Capsicum", "Vegetable"},
                                {"Cauliflower", "Vegetable"},
                                {"Orange", "Fruit"},
                                {"Strawberry", "Fruit"},
                                {"Cherry", "Fruit"},
                                {"Raspberry", "Fruit"},
                                {"Lettuce", "Vegetable"},
                                {"Cucumber", "Vegetable"},
                                {"Watermelon", "Fruit"},
                                {"Beetroot", "Vegetable"},
                            };
                            
                            int vegetableCount = 0;
                            int fruitCount = 0;

                            foreach (string fruitOrVegetable in fruitAndVegetables.Values) //Counts how many times a value comes up and adds it to the integers above
                            {
                                if (fruitOrVegetable == "Vegetable") vegetableCount++;
                                if (fruitOrVegetable == "Fruit") fruitCount++;
                            }
                            
                            Console.WriteLine($"\nThe number of vegetables is {vegetableCount}");
                            Console.WriteLine($"The number of fruits is {fruitCount}");
                            
                            Console.WriteLine("\nList display of all fruits: \n");
                            foreach (KeyValuePair<string, string> fruitList in fruitAndVegetables)
                            {
                                if (fruitList.Value == "Fruit") Console.WriteLine("{0} is a {1}", fruitList.Key, fruitList.Value);
                            }
                            
                            Console.WriteLine("\nPress <Enter> to return to menu"); 
                            Console.ReadLine(); //Question 3 ends
                            

                        break; //break; is required between case (before [except for case 1] and after, think of it like brackets)

                        case 4:
                            Console.WriteLine($"You selected option {selection}");
                            var carList = new Car<string, string>();
            
                            carList.addCar("540i", "BMW");
                            carList.addCar("RS6", "Audi");
                            carList.addCar("328i", "BMW");
                            carList.addCar("Pulsar", "Nissan");
                            carList.addCar("M2", "BMW");
                            carList.addCar("Golf", "VW");
                            carList.addCar("Amarok", "VW");
                            carList.addCar("S4", "Audi");
                            carList.addCar("M3", "BMW");
                            carList.addCar("Skyline", "Nissan");

                            carList.ForEach(Console.WriteLine);

                            carList.Sort((a,b) => a.Item2.CompareTo(b.Item2));
                            Console.WriteLine();
                            carList.ForEach(Console.WriteLine);
        
                            Console.WriteLine("Press <Enter> to return to menu"); 
                            Console.ReadKey();
                        break;

                        case 5://Quit program
                            Console.WriteLine("Thanks for using the program");
<<<<<<< HEAD
                        goto End;
=======
                        break;

                        default:
                            Console.WriteLine($"You selected option {selection}");
                            Console.WriteLine("Press <Enter> to return to menu"); 
                            Console.ReadKey();
                        break;
>>>>>>> 7460bfedd2fdcf8be449284d6c8cab465100c9cd
                    }
                }
                else
                {
                    Console.WriteLine("ERROR: You didn't type in a number");
                    selection = 0;
<<<<<<< HEAD
                    Console.WriteLine("Press <Enter> to try again.");
=======
                    Console.WriteLine("Press <Enter> to return to menu");
>>>>>>> 7460bfedd2fdcf8be449284d6c8cab465100c9cd
                    Console.ReadKey();
                }

                Console.WriteLine();

            }while(selection < 5);

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine($"ERROR: There is no entry for option '{selection}' \nPress <Enter> to try again.");
            Console.ReadKey();
            goto Start;

            End:
            Console.WriteLine("Press <Enter> to quit the program.");
            Console.ReadKey();
     
        }
    }
}

